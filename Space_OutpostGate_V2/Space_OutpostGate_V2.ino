#include <Wire.h>
#define BLYNK_PRINT Serial
#include <ESP8266_Lib.h>
#include <BlynkSimpleShieldEsp8266.h>
#include <TimeLib.h>
//#include <WidgetRTC.h>
#include <SoftwareSerial.h>
#include <SPI.h>
#include <LiquidCrystal.h>
#include <MFRC522.h>
#include <Servo.h>


// defines arduino trig and echo pins numbers
#define trig_pin  24
#define echo_pin  25
#define trig_pin2  26
#define echo_pin2  27
#define trig_pin3  28
#define echo_pin3  29
#define led_r1   22
#define led_r2  33
#define led_r3   40
#define led_g1  23
#define led_g2  32
#define led_g3  41
#define EspSerial Serial1
#define ESP8266_BAUD 115200
#define SS_PIN 53
#define RST_PIN 5
#define LED_G 6 //define green LED pin
#define LED_R 4 //define red LED
#define BUZZER 2 //buzzer pin
#define Peyas "71 1E A3 C3" // define main card with uid
#define Swapnil "43 EA 0D 32" // define card1 with uid
#define Rimi "6B D3 99 1B" // difine tag with uid

MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.
Servo myServo; //define servo name



// defines variables
int space,sensor_1,sensor_2,sensor_3;
int count=0;
byte availSlot=0;

LiquidCrystal lcd(38,39,34,35,36,37);

// You should get Auth Token in the Blynk App.
// Go to the Project Settings (nut icon).
char auth[] = "kZyZJ3N_Qq5HPJq0p47x0p_EnIznwT1n";

// Your WiFi credentials.
// Set password to "" for open networks.
char ssid[] = "Md. monir Hossain";
char pass[] = "0123456789";

// Hardware Serial on Mega, Leonardo, Micro...

ESP8266 wifi(&EspSerial);

BlynkTimer timer;
WidgetMap myMap(V7);

WidgetLED led1_red(V22);
WidgetLED led1_green(V23);
WidgetLED led2_red(V32);
WidgetLED led2_green(V33);
WidgetLED led3_red(V40);
WidgetLED led3_green(V41);


void setup() 
{  
  lcd.begin(16,2);
  Wire.begin();
  pinMode(trig_pin, OUTPUT); // Sets the trigPin as an Output
  pinMode(echo_pin, INPUT); // Sets the echoPin as an Input
  
  pinMode(trig_pin2, OUTPUT); // Sets the trigPin as an Output
  pinMode(echo_pin2, INPUT); // Sets the echoPin as an Input
  
  pinMode(trig_pin3, OUTPUT); // Sets the trigPin as an Output
  pinMode(echo_pin3, INPUT); // Sets the echoPin as an Input
  
  Serial.begin(9600); // Starts the serial communication
  SPI.begin();      // Initiate  SPI bus
  mfrc522.PCD_Init();   // Initiate MFRC522
  myServo.attach(3); //servo pin
  myServo.write(150); //servo start position
  pinMode(LED_G, OUTPUT);
  pinMode(LED_R, OUTPUT);
  pinMode(BUZZER, OUTPUT);
  noTone(BUZZER);
  Serial.println("Put your card to the reader...");
  Serial.println();
  
  //start message pop up

  // Set ESP8266 baud rate
  EspSerial.begin(ESP8266_BAUD);
  delay(10);
  Blynk.begin(auth, wifi, ssid, pass);
  start();
  
  int index = 0;
  float lat = 23.8151;
  float lon = 90.4255;
  myMap.location(index, lat, lon, "value");

   
}




void start() // strating message pop up 
{  
  lcd.setCursor(0,0);
  lcd.print("RFID-IoT BASED");
  Serial.println("RFID-IoT BASED");
  lcd.setCursor(0,1);
  lcd.print("SMART CAR PARk..");
  Serial.println("SMART CAR PARk..");
  delay(4000);
  lcd.clear();
  
  lcd.setCursor(0,0);
  lcd.print("System Module");
  lcd.setCursor(0,1);
  lcd.print("Initializing....");
  Serial.println("System Initializing...");
  delay(3000);
  lcd.clear();
 }



int spaceCheck(long period)
{ // Calculating the distance 
  //speed of sound v=340 m/s;
  //v=0,034cm/microSeconds(2)
  //time= distance/speed
  //distance= time*speed
  
  float velocity= 0.0343/2;
   space = period*velocity;
  return space;
 }


int Sensor(int tp,int ep)
{ long duration, distance;
  digitalWrite(tp, LOW);// Clears the trigPin
  delayMicroseconds(2);
  digitalWrite(tp, HIGH); 
  delayMicroseconds(10);// Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(tp, LOW); //giving pause after 10 mico second
  
  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(ep, HIGH);
  distance = spaceCheck(duration);
 
 /* Prints the distance on the Serial Monitor
    Serial.println("Distance read from Sensor = ");
    Serial.print(distance);
    Serial.println(" cm");*/
    delay(150);
    return distance; 
}

void SlotCount()
{
   Sensor(trig_pin,echo_pin);
   Sensor(trig_pin2,echo_pin2);
   Sensor(trig_pin3,echo_pin3);
  
   if (Sensor(trig_pin,echo_pin) <= 10)
     {
       sensor_1 = 1;
       digitalWrite(led_r1, HIGH);
       led1_red.on();
       digitalWrite(led_g1, LOW);
       led1_green.off();
     }
   else  {
    sensor_1 = 0;
    digitalWrite(led_r1, LOW);
    led1_red.off();
    digitalWrite(led_g1, HIGH);
    led1_green.on();
   }

   if (Sensor(trig_pin2,echo_pin2) <= 10)
     {
       sensor_2 = 1;
       digitalWrite(led_r2, HIGH);
        led2_red.on();
       digitalWrite(led_g2, LOW);
       led2_green.off();
     }
   else {
    sensor_2 = 0;
    digitalWrite(led_r2, LOW);
    led2_red.off();
    digitalWrite(led_g2, HIGH);
    led2_green.on();
   }

   if (Sensor(trig_pin3,echo_pin3) <= 10)
     {
       sensor_3 = 1;
       digitalWrite(led_r3, HIGH);
        led3_red.on();
       digitalWrite(led_g3, LOW);
       led3_green.off();
     }
   else  {
    sensor_3 = 0;
    digitalWrite(led_r3, LOW);
    led3_red.off();
    digitalWrite(led_g3, HIGH);
    led3_green.on();
   }

   count = sensor_1 + sensor_2 + sensor_3; //counting non Available slot
   availSlot = 3 - count;
   Serial.println("available parking slot: ");

   Serial.println(availSlot);
   lcd.setCursor(0,0);
   lcd.print ("Available Free");
   lcd.setCursor(0,1);
   lcd.print ("Parking slot: ");
  
   lcd.print (availSlot);
   Blynk.virtualWrite(V6,   availSlot);
   
    Wire.beginTransmission(8);
    Wire.write("Slot is ");
    Wire.write(availSlot);
    Wire.endTransmission();
     delay(200);
     
   sensor_1 = 0;
   sensor_2 = 0;
   sensor_3 = 0; 
  
  }


void rfid() 
{
  // Look for new cards
  if ( ! mfrc522.PICC_IsNewCardPresent()) 
  {
    return;
  }
  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial()) 
  {
    return;
  }

  
  //Show UID on serial monitor
  Serial.print("UID tag :");
  String content= "";
  byte letter;
  for (byte i = 0; i < mfrc522.uid.size; i++) 
  {
     Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
     Serial.print(mfrc522.uid.uidByte[i], HEX);
     content.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));
     content.concat(String(mfrc522.uid.uidByte[i], HEX));
  }
  
  content.toUpperCase();
  if (content.substring(1)== Monir)
   {
    Serial.println ("Name: Md.  Monir Hossain");
   }
   
   else if (content.substring(1) == Reazul)
   {
   Serial.println ("Name: Reazul Alam");
   }
   else if (content.substring(1) == Monira )
   {
   Serial.println ("Name: Monira Mustakim");
   }
   else
   {
   Serial.println("Name:______________");
    }

   
  
  Serial.println();
  Serial.print("Message : ");

  if (content.substring(1) == Peyas || content.substring(1) == Swapnil || content.substring(1) == Rimi ) //change here the UID of the card/cards  to give access
  {
    Serial.println("Authorized access");
    Serial.println();
    delay(500);
    digitalWrite(LED_G, HIGH);
    tone(BUZZER, 900);
    delay(300);
    noTone(BUZZER);
    myServo.write(0);
    delay(5000);
    myServo.write(150);
    digitalWrite(LED_G, LOW);
  }
 
 else   {
    Serial.println(" Access denied");
    Serial.println();
    digitalWrite(LED_R, HIGH);
    tone(BUZZER, 700);
    delay(1000);
    digitalWrite(LED_R, LOW);
    noTone(BUZZER);
  }
}



void loop()
{  
    Blynk.run();
    timer.run(); // Initiates BlynkTimer
    
   timer.setInterval(1000L, SlotCount);
   timer.setInterval(0L, rfid);
}
