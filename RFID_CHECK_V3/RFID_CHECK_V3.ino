#include <SoftwareSerial.h>
#include <SPI.h>
#include <MFRC522.h>
#include <Servo.h>
#include <LiquidCrystal.h>
#include <Wire.h>
 
#define SDA_PIN 53
#define RST_PIN 5
#define LED_G 6 //define green LED pin
#define LED_R 4 //define red LED
#define BUZZER 2 //buzzer pin
#define Monir "71 1E A3 C3" // define main card with uid
#define Reazul "43 EA 0D 32" // define card1 with uid
#define Monira "6B D3 99 1B" // difine tag with uid


LiquidCrystal lcd(48,49,44,45,46,47);   //defining lcd pins (rs,r/w,d4,d5,d6,d7)
MFRC522 mfrc522(SDA_PIN, RST_PIN);   // Create MFRC522 instance.
Servo myServo; //define servo name

 int balanceMonir=20;
 int balanceReazul=15;
 int balanceMonira= 4;
 int minBalance=5;

void start() // strating message pop up 
{
  lcd.setCursor(0,0);
  lcd.print("RFID-IoT BASED");
  Serial.println("RFID-IoT BASED");
  lcd.setCursor(0,1);
  lcd.print("SMART CAR PARk..");
  Serial.println("SMART CAR PARk..");
  delay(5000);
  lcd.clear();
  
  lcd.setCursor(0,0);
  lcd.print("System Module");
  lcd.setCursor(0,1);
  lcd.print("Initializing....");
  Serial.println("System Initializing....");
  delay(4000);
  lcd.clear(); 

  lcd.setCursor(0,0);
  lcd.print("Put your card to");
  lcd.setCursor(0,1);
  lcd.print("the reader...");
  Serial.println("Put your card to the reader...");
  Serial.println();
 }



  
String uid() // print uuid tag number to match 
 {     
  

        
    Serial.print("UID tag :");
    String content= "";
    byte letter;
     for (byte i = 0; i < mfrc522.uid.size; i++) 
       {
          Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
          Serial.print(mfrc522.uid.uidByte[i], HEX);
          content.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));
          content.concat(String(mfrc522.uid.uidByte[i], HEX));
       } 
       return content;
 }




void RFID_check(String content)  //validation check of rfid and balance requirement check;
{
  content.toUpperCase();
  if (content.substring(1)== Monir && balanceMonir>=minBalance)
   {
    Serial.println ("Name: Md. Monir Hossain");
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print ("Name: MR.Monir");
    
    Serial.println();
    Serial.print("Message : ");
    Serial.println("Authorized access");
    Serial.println();
    lcd.setCursor(0,1);
    lcd.print("Permit: Approved");
    delay(1500);
    
    balanceMonir = balanceMonir - minBalance;

    lcd.setCursor(0,1);
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print ("Name: MR.Monir");
    lcd.setCursor(0,1);
    lcd.print("N.Balance:");
    lcd.print(balanceMonir);
    lcd.print("$");

    digitalWrite(LED_G, HIGH);
    tone(BUZZER, 500);
    delay(400);
    noTone(BUZZER);
    myServo.write(180);
    delay(5000);
    myServo.write(0);
    digitalWrite(LED_G, LOW);

    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Put your card to");
    lcd.setCursor(0,1);
    lcd.print("the reader...");
    Serial.println("Put your card to the reader...");
    Serial.println();

   }

  else if (content.substring(1)== Monir && balanceMonir < minBalance)
   {
    Serial.println ("Name: Md. Monir Hossain");
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print ("Name: MR.Monir");
    
    Serial.println();
    Serial.print("Message : ");
    Serial.println("please renew your card balance is not sufficient");
    Serial.println();
    lcd.setCursor(0,1);
    lcd.print("Renew your card");
    delay(2000);
    
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print ("Name: MR.Monir");
    lcd.setCursor(0,1);
    lcd.print("Balance is NILL");
    
    digitalWrite(LED_R, HIGH);
    tone(BUZZER, 300);
    delay(2000);
    digitalWrite(LED_R, LOW);
    noTone(BUZZER);
    
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Put your card to");
    lcd.setCursor(0,1);
    lcd.print("the reader...");
    Serial.println("Put your card to the reader...");
    Serial.println();

   }
   
   else if (content.substring(1) == Reazul && balanceReazul>=minBalance)
   {
    Serial.println ("Name: Md. Reazul Alam");
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Name: MR.Reazul");
    
    Serial.println();
    Serial.print("Message : ");
    Serial.println("Authorized access");
    Serial.println();
    lcd.setCursor(0,1);
    lcd.print("Permit: Approved");
    delay(1500);

    balanceReazul = balanceReazul - minBalance;

    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Name: Mr.Reazul");
    lcd.setCursor(0,1);
    lcd.print("N.Balance:");
    lcd.print(balanceReazul);
    lcd.print("$");

    digitalWrite(LED_G, HIGH);
    tone(BUZZER, 500);
    delay(400);
    noTone(BUZZER);
    myServo.write(180);
    delay(5000);
    myServo.write(0);
    digitalWrite(LED_G, LOW);

    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Put your card to");
    lcd.setCursor(0,1);
    lcd.print("the reader...");
    Serial.println("Put your card to the reader...");
    Serial.println();

    
   }
  
   else if (content.substring(1) == Reazul && balanceReazul < minBalance)
   {
    Serial.println ("Name: Md. Reazul Alam");
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print ("Name: MR.Reazul");
    
    Serial.println();
    Serial.print("Message : ");
    Serial.println("please renew your card balance is not sufficient");
    Serial.println();
    lcd.setCursor(0,1);
    lcd.print("Renew your card");
    delay(2000);
    
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print ("Name: MR.Reazul");
    lcd.setCursor(0,1);
    lcd.print("Balance is NILL");
    
    digitalWrite(LED_R, HIGH);
    tone(BUZZER, 300);
    delay(2000);
    digitalWrite(LED_R, LOW);
    noTone(BUZZER);
    
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Put your card to");
    lcd.setCursor(0,1);
    lcd.print("the reader...");
    Serial.println("Put your card to the reader...");
    Serial.println();

   }

   
   else if (content.substring(1) == Monira && balanceMonira>=minBalance )
   {
    Serial.println ("Name: Monira Mustakim");
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Name: Miss.Monira");

    Serial.println();
    Serial.print("Message : ");
    Serial.println("Authorized access");
    Serial.println();
    lcd.setCursor(0,1);
    lcd.print("Permit: Approved");
    delay(1500);
    
    balanceRimi = balanceRimi - minBalance;

    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Name: Miss.Monira");
    lcd.setCursor(0,1);
    lcd.print("N.Balance:");
    lcd.print(balanceMonira);
    lcd.print("$");

    digitalWrite(LED_G, HIGH);
    tone(BUZZER, 500);
    delay(400);
    noTone(BUZZER);
    myServo.write(180);
    delay(5000);
    myServo.write(0);
    digitalWrite(LED_G, LOW);

    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Put your card to");
    lcd.setCursor(0,1);
    lcd.print("the reader...");
    Serial.println("Put your card to the reader...");
    Serial.println();

   }


   else if (content.substring(1) == Monira && balanceMonira < minBalance)
   {
    Serial.println ("Name: Monira Mustakim");
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print ("Name: Miss.Monira");
    
    Serial.println();
    Serial.print("Message : ");
    Serial.println("please renew your card balance is not sufficient");
    Serial.println();
    lcd.setCursor(0,1);
    lcd.print("Renew your card");
    delay(2000);
    
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print ("Name: Miss.Monira");
    lcd.setCursor(0,1);
    lcd.print("Balance is NILL");
    
    digitalWrite(LED_R, HIGH);
    tone(BUZZER, 300);
    delay(2000);
    digitalWrite(LED_R, LOW);
    noTone(BUZZER);
    
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Put your card to");
    lcd.setCursor(0,1);
    lcd.print("the reader...");
    Serial.println("Put your card to the reader...");
    Serial.println();

   }
   
   else
    { lcd.clear();
     lcd.setCursor(0,0);
     lcd.print("Name:______  ");
     Serial.println("Name:______________");
     denied();
     delay(3000);

     lcd.clear();
     lcd.setCursor(0,0);
     lcd.print("Put your card to");
     lcd.setCursor(0,1);
     lcd.print("the reader...");
     Serial.println("Put your card to the reader...");
     Serial.println() ;
    }
}





void denied() //when authorized denied
{   Serial.println();
    Serial.print("Message : ");
    Serial.println(" Access denied");
    Serial.println();
    lcd.setCursor(0,1);
    lcd.print("Permit: Failed");
    delay(1000);
    digitalWrite(LED_R, HIGH);
    tone(BUZZER, 300);
    delay(1000);
    digitalWrite(LED_R, LOW);
    noTone(BUZZER);
}





 
void setup() 
{
  pinMode(49,INPUT);      //setting arduino pin3 as input
  Serial.begin(9600);   // Initiate a serial communication.opens serial port, sets data rate to 9600 bps
  lcd.begin(16,2);      // set up the LCD's number of columns and rows
 
  SPI.begin();      // Initiate  SPI bus
  mfrc522.PCD_Init();   // Initiate MFRC522
  myServo.attach(3); //servo pin
  myServo.write(0); //servo start position
  pinMode(LED_G, OUTPUT);
  pinMode(LED_R, OUTPUT);
  pinMode(BUZZER, OUTPUT);
  noTone(BUZZER);
  start();
}


void loop() 
{   
  // Look for new cards
      if ( ! mfrc522.PICC_IsNewCardPresent()) 
       {
          return;
       }
       // Select one of the cards
      if ( ! mfrc522.PICC_ReadCardSerial()) 
        {
          return;
        }
    
     uid();       //Show UID on serial monitor
     
     RFID_check(uid()); //checking validation of the rfid
  
}
